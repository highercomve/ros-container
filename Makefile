all: help

help:
	@echo ""
	@echo "-- Help Menu"
	@echo ""
	@echo "   1. make build            - build all images"
	@echo "   2. make pull             - pull all images"
	@echo "   3. make clean            - remove all images"
	@echo ""

build:
	@docker buildx build --platform linux/arm64,linux/amd64 --push --tag=highercomve/ros:eloquent-ros-core ros-core/
	@docker buildx build --platform linux/arm64,linux/amd64 --push --tag=highercomve/ros:eloquent-ros-base ros-base/
	@docker buildx build --platform linux/arm64,linux/amd64 --push --tag=highercomve/ros:eloquent-ros1-bridge ros1-bridge/
	@docker buildx build --platform linux/arm64,linux/amd64 --push --tag=highercomve/ros:eloquent-desktop desktop/

pull:
	@docker pull highercomve/ros:eloquent-ros-core
	@docker pull highercomve/ros:eloquent-ros-base
	@docker pull highercomve/ros:eloquent-ros1-bridge
	@docker pull highercomve/ros:eloquent-desktop

clean:
	@docker rmi -f highercomve/ros:eloquent-ros-core
	@docker rmi -f highercomve/ros:eloquent-ros-base
	@docker rmi -f highercomve/ros:eloquent-ros1-bridge
	@docker rmi -f highercomve/ros:eloquent-desktop
